// Задание
/*
1 Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
2 Создать массив элементов 'название месяцов' содержащий названия месяцев
3 Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)
4 Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
5 Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
6 Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)
7 Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года
*/
//1 
let days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
//2
let months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
//3
for i in days{
    print(i)
}
//4
for i in 0..<months.count{
    print(months[i], days[i])
}
//5
var dayInMonths: [(String, Int)] = []

for i in 0..<months.count{
    dayInMonths += [(month: months[i], days: days[i])]
}

for (month, day) in dayInMonths{
    print(month,day)
}
//6
for i in stride(from: months.count-1, to: 0, by: -1){
    print(months[i], days[i])
}
//7 Date - 5 May
let date = ( day: 5, month: "May")
var counter = (i:0,sum:0)

while dayInMonths[counter.i].0 != date.month{
  counter.sum += dayInMonths[counter.i].1
  counter.i += 1
}
counter.sum += date.day - 1
print(counter.sum)
